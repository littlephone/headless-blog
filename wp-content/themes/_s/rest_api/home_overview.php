<?php

//Hooks to get article by slug.
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/home', array(
        'methods' => 'GET',
        'callback' => 'rest_get_home_overview',
    ));
});

function rest_get_home_overview($data)
{
    $data = array(
        'hero' => wp_get_attachment_image_url(carbon_get_post_meta(get_option('page_on_front'), 'hero'), '1920_1080') ,
    );
    return $data;

}