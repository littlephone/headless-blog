<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'post_fields' );

function post_fields(){
    Container::make( 'post_meta', __( 'Post Fields', 'nothing') )
        ->add_fields( array(
            Field::make('complex', 'updates', __('Post Fields', 'nothing') )
                ->add_fields(array(
                    Field::make('rich_text', 'update',  'Update'),
                    Field::make('date_time', 'update_date', 'Date and Time')
                        ->set_default_value(date('Y-m-d H:i:s'))
                        ->set_input_format('Y-m-d H:i:s', 'Y-m-d H:i:S')
                        ->set_picker_options(
                            array(
                                'time_24hr' => true,
                            )
                        )

                ))
        ) )
    ->where('post_type', '=', 'post');
}