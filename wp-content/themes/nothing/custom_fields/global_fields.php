<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'global_fields_options' );
add_action( 'carbon_fields_register_fields', 'about_options' );

function global_fields_options() {
    Container::make( 'theme_options', __( 'Global Fields', 'nothing') )
        ->add_fields( array(
            Field::make('complex', 'crb_global_posts', __('Latest Post Configuration', 'nothing') )
                ->set_min(1)
                ->set_max(1)
                ->add_fields(array(
                    Field::make('text', 'num_latest_post',  'Number of Post')
                        ->set_default_value('3')
                        ->set_attribute('type', 'number')
                )),
        ) );
}

function about_options() {
    Container::make( 'post_meta', __( 'About Us Fields', 'nothing') )
        ->add_fields( array(
                Field::make( 'image', 'profile_image', __( 'Profile image' ) ),
                Field::make('text', 'name')->set_width( 50 ),
                Field::make('text', 'twitter')->set_width( 50 ),
                Field::make('text', 'linkedin')->set_width( 50 ),
                Field::make('text', 'gitlab')->set_width( 50 ),
                Field::make('complex', 'technologies', __('Favourite technologies', 'nothing') )
                    ->add_fields(array(
                        Field::make('text', 'technology')
                    ))
                    ->set_layout('tabbed-horizontal'),

                Field::make('complex', 'work_experience', __('Work Experience', 'nothing'))
                    ->add_fields(array(
                        Field::make('text', 'organisation', __('Company / Organisation', 'nothing'))->set_width( 33 ),
                        Field::make('text', 'date_from', __('From', 'nothing'))->set_width( 33 ),
                        Field::make('text', 'date_to', __('To', 'nothing'))->set_width( 33)
                            ->set_conditional_logic( array(
                                array(
                                    'field' => 'current',
                                    'value' => false,
                                )
                            ) ),
                        Field::make('checkbox', 'current', __('Current Job ?')),
                        Field::make('textarea', 'experience_introduction', __('Experience Introduction'))
                            ->set_rows( 5 ),
                    )),

                Field::make('complex', 'projects_highlight', __('Project Highlights', 'nothing'))
                    ->add_fields(array(
                        Field::make('text', 'project_title', __('Project Title', 'nothing'))->set_width( 50),
                        Field::make('image', 'project_image', __('Project Image', 'nothing'))->set_width( 50),
                        Field::make('textarea', 'project_introduction', __('Project Introduction', 'nothing'))->set_width( 50),
                    ))

        )
        )
        ->where('post_type', '=', 'page')
        ->where('post_id', '=', get_page_by_path('about')->ID);
}