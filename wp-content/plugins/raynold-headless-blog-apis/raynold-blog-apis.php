<?php
/*
  Plugin Name: Raynold Headless Blog APIs
  Plugin URI: https://raynoldchow.dev/
  Description: An standard API tool for registering API endpoint files
  Author: Raynold
  Author URI: https://raynoldchow.dev/
  Text Domain: raynold-headless-blog-apis
  Version: 1.0.20210417
*/

define('RAYNOLD_PLUGIN_BASE_PATH', plugin_dir_path(__FILE__));
define('RAYNOLD_PLUGIN_BASE_URL', plugin_dir_url(__FILE__));

include_once RAYNOLD_PLUGIN_BASE_PATH . '/src/class-raynold-headless-rendering.php';

if( ! class_exists('RaynoldBlogAPI') ){
    class RaynoldBlogAPI{
        public function __construct()
        {
            //Automatically register API Endpoints
            foreach (glob(RAYNOLD_PLUGIN_BASE_PATH . 'rest_api/*.php') as $raynold_endpoint_scripts) {
                include $raynold_endpoint_scripts;
            }
        }
    }
}

if(! function_exists('RaynoldBlogAPI')){
    function RaynoldBlogAPI() {
        global $raynold_api;
        if(empty($raynold_api)){
            $raynold_api = new RaynoldBlogAPI();
        }
    }
}

RaynoldBlogAPI();