<?php

if( ! class_exists('RaynoldHeadlessRendering') ) {
    class RaynoldHeadlessRendering
    {
        public function post_filter(WP_Query $wp_query)
        {
            $post_data = array();
            if ($wp_query->have_posts()):
                while ($wp_query->have_posts()) : $wp_query->the_post();
                    $post_data[] = array(
                        'ID' => get_the_ID(),
                        'title' => get_the_title(),
                        'slug' => get_post_field('post_name', get_the_ID()),
                        'date' => get_the_date('c'),
                        'excerpt' => get_the_excerpt(),
                        'content' => apply_filters('the_content', get_the_content()),
                        'thumbnail' => wp_get_attachment_image_url(get_post_thumbnail_id(), '1920_1080'),
                        'thumbnail_srcset' => wp_get_attachment_image_srcset(get_post_thumbnail_id() , '1920_1080'),
                    );
                endwhile;
            endif;

            return $post_data;
        }

        public function enforce_error_roadblock($data) {
            $remote_ip = $_SERVER['REMOTE_ADDR'];
            if(!empty($data['error'])){
                $attempt = array(
                    'trials' => 0,
                );
                $expiry = 0;
                if ($attempt_server = get_transient('login_attempts_' . $remote_ip)) {
                    $attempt['trials'] = $attempt_server['trials'];
                    $expiry = (int) (get_option( '_transient_timeout_login_attempts_' . $remote_ip , 0) - time() - 600) / 60;
                }

                $attempt['trials']++;

                $remaining_trials = 3 - $attempt['trials'];
                if($remaining_trials <= 0 ){
                    $valid_time = (int) $expiry +  pow(2, abs($remaining_trials)) * 600;
                    $data['error'] = array(
                        'error_msg' => $data['error'],
                        'remaining_time' => (int) ($valid_time / 60),
                    );
                }else{
                    $valid_time = 600;
                    $data['error'] = array(
                        'error_msg' => $data['error'],
                        'remaining_trials' => $remaining_trials,
                    );
                }
                set_transient('login_attempts_' . $remote_ip , $attempt, $valid_time);
            }
            return $data;
        }

        public function post_details_filter(WP_Query $wp_query)
        {
            //Although it's useless, but we have to use loop. It's due to WordPress's limitation
            $data = [];
            while($wp_query->have_posts()): $wp_query->the_post();
                $category_arr = get_the_category(get_the_ID());
                $data = [
                    'ID' => get_the_ID(),
                    'title' => get_post_field('post_title', get_the_ID()),
                    'slug' =>  get_post_field('post_name', get_the_ID()),
                    'content' => apply_filters('the_content', get_the_content()),
                    'date' => get_the_date('c'),
                    'comment_open' => comments_open(get_the_ID()),
                    'thumbnail' => wp_get_attachment_image_url(get_post_thumbnail_id(), '1920_1080'),
                    'thumbnail_srcset' => wp_get_attachment_image_srcset(get_post_thumbnail_id() , '1920_1080'),
                    'update' => array(),
                ];
                if(carbon_get_post_meta(get_the_ID(), 'updates')){
                    foreach(carbon_get_post_meta(get_the_ID(), 'updates') as $update){
                        $data['update'][] = array(
                            'content' => $update['update'],
                            'date' => date('c', strtotime($update['update_date'])),
                        );
                    }
                }
                foreach($category_arr as $category){
                    $data['category'][] = array(
                        'name' => $category->name,
                        'slug' => $category->slug,
                    );
                }
            endwhile;
            return $data;
        }

        protected function getHumanTimeRepresentation($seconds){

        }
    }
}