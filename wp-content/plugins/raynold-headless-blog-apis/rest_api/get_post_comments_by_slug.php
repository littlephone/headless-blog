<?php
//Hooks to get article by slug.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/comments/(?P<slug>\S+)', array(
        'methods' => 'GET',
        'callback' => 'rest_get_post_comments_by_slug',
    ));
});

function rest_get_post_comments_by_slug($data) {
    $post_query = get_posts([
        'post_type' => 'post',
        'name' => $data['slug'],
    ]);

    if(empty($post_query)){
        return new WP_Error( 'no_such_post', 'No such post', array( 'status' => 404 ) );
    }

    $post_id = $post_query[0]->ID;
    $query = new WP_Comment_Query([
        'post_id' => $post_id,
    ]);
    $comments = $query->comments;

    $data = array(
        'comments' => array()
    );

    if(!empty($comments)){
        foreach($comments as $comment){
            $data['comments'][] = array(
                'author' => ($comment->comment_approved) ? $comment->comment_author: 'Pending Approval',
                'date' =>  get_comment_date('c', $comment),
                'content' =>  ($comment->comment_approved) ? $comment->comment_content : 'Comment pending approval.',
            );
        }
    }


    return $data;
}