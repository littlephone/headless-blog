<?php

//Hooks to login
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/profile/', array(
        'methods' => 'GET',
        'callback' => 'api_profile',
    ));
});

function api_profile() {
    var_dump(getallheaders()['X-WP-Nonce']);

    if(!is_user_logged_in()) {
       $data['error'] = array(
           'message' => __('Please login before continuing'),
           'is_logged_in' => false,
       );

        return new WP_REST_Response($data, 403);
    }
    $current_user = wp_get_current_user();
    $data['user'] = array(
        'id' => $current_user->ID,
        'username' => $current_user->user_login,
        'email' => $current_user->user_email,
        'roles' => $current_user->roles,
    );
    return $data;
}