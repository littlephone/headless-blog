<?php
//Hooks to register.
add_action('rest_api_init', function(){
    register_rest_route('headless-blog/v1', '/register', array(
        'methods' => 'POST',
        'callback' => 'rest_register',
    ));
});

function rest_register($request) {
    $username = $request['username'];
    $password = $request['password'];
    $repassword = $request['repassword'];
    $email = $request['email'];

    $error_fields = [];

    if(empty($username)){
        $error_fields['username'] = 'Username cannot be empty';
    }
    if(empty($password)){
        $error_fields['password'] = 'Password cannot be empty';
    }
    if(empty($repassword)){
        $error_fields['repassword'] = 'Please type your password again to verify.';
    }
    if(empty($email)) {
        $error_fields['email'] = 'Email cannot be empty';
    }
    //Check email validity
    $email_arr = explode('@', $email);
    if(!checkdnsrr($email_arr[1], 'MX')){
        $error_fields['email'] = 'The email address isn\'t valid.';
    }

    if(strcmp($password, $repassword)) {
        $error_fields['repassword'] = 'Your password inputs doesn\'t match.';
    }

    if(!empty($error_fields)){
        $data = array(
            'success' => false,
            'message' => $error_fields,
        );
    } else{
        $user = wp_create_user($username, $password, $email);
        if(!is_wp_error($user)) {
            $data = array(
                'success' => true,
                'id' => $user,
                'message' => 'The account has been registered and pending approval from the site owner.',
            );
        }else{
            $data = array(
                'success' => false,
                'message' => $user->get_error_message(),
            );
        }
    }


    return $data;
}