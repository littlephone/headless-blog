<?php
//Hooks to get category
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/category/', array(
        'methods' => 'GET',
        'callback' => 'rest_get_post_categories',
    ));
});

function rest_get_post_categories()
{
    $request = new WP_REST_Request();
    $queryParam = $request->get_query_params();

    $category_term = get_terms(
        [
            'taxonomy' => 'category',
            'hide_empty' => true,
        ]
    );

    $term_arr = array();

    foreach($category_term as $term){
        $term_arr[] = array(
            'name' => $term->name,
            'slug' => $term->slug,
            'post_count' => $term->count,
        );
    }
    return $term_arr;
}