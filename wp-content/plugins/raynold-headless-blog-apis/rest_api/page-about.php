<?php

//Hooks to get latest article
add_action('rest_api_init', function () {
    register_rest_route('headless-blog/v1', '/about', array(
        'methods' => 'GET',
        'callback' => 'about_content',
    ));
});

function about_content() {
    $post = get_page_by_path('about');
    $post_id = get_page_by_path('about')->ID;
    $data = array(
        'profile_image' => wp_get_attachment_image_url(
            carbon_get_post_meta($post_id, 'profile_image'), '300_300') ,

        'name' => carbon_get_post_meta($post_id, 'name'),
        'social' => array(
            'twitter' => carbon_get_post_meta($post_id, 'twitter'),
            'linkedin' => carbon_get_post_meta($post_id, 'linkedin'),
            'gitlab' => carbon_get_post_meta($post_id, 'gitlab'),
        ),
        'introduction' => apply_filters('the_content', $post->post_content),
    );
    foreach(carbon_get_post_meta($post_id, 'technologies') as $technology){
        $data['technology'][] = $technology['technology'];
    }
    foreach(carbon_get_post_meta($post_id, 'work_experience') as $experience){
        $data['experience'][] = array(
            'organisation' => $experience['organisation'],
            'from' => date('Y-m-d H:i:s', strtotime($experience['date_from'])),
            'to' => (!empty($experience['date_to'])) ? date('Y-m-d H:i:s', strtotime($experience['date_to'])) : '',
            'introduction' => $experience['experience_introduction'],
        );
    }
    foreach(carbon_get_post_meta($post_id, 'projects_highlight') as $project){
        $data['projects'][] = array(
            'title' => $project['project_title'],
            'image' => wp_get_attachment_image_url($project['project_image'], '400_300') ,
            'introduction' => $project['project_introduction'],
        );
    }
    return $data;
}

